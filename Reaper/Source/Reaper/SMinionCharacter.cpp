// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "SMinionCharacter.h"
#include "MinionAIController.h"

void ASMinionCharacter::MoveToTarget(AActor * target)
{
	AMinionAIController *Controller = Cast<AMinionAIController>(GetController());
	if (Controller)
	{
		Controller->SetTargetPoint(target);
	}
}
