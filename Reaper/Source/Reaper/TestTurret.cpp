// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "TestTurret.h"
#include "SZombieCharacter.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ATestTurret::ATestTurret(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	MuzzleAttachPoint = TEXT("MuzzleFlashSocket");

	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, ACharacter::CapsuleComponentName);
	CapsuleComponent->InitCapsuleSize(65.f, 65.f);
	CapsuleComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);

	CapsuleComponent->CanCharacterStepUpOn = ECB_No;
	CapsuleComponent->bShouldUpdatePhysicsVolume = true;
	CapsuleComponent->bCheckAsyncSceneOnMove = false;
	CapsuleComponent->SetCanEverAffectNavigation(false);
	CapsuleComponent->bDynamicObstacle = true;
	RootComponent = CapsuleComponent;

	SkeletalMeshComponent = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("Mesh"));
	SkeletalMeshComponent->SetupAttachment(RootComponent);

	
	SphereComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("Sphere"));
	SphereComponent->SetRelativeLocation(FVector(0, 0, 0));
	SphereComponent->CanCharacterStepUpOn = ECB_No;
	SphereComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComponent->SetupAttachment(RootComponent);

	WeaponData.Damage = 40;
	WeaponData.MaxAmmo = 999;
	WeaponData.StartAmmo = 999;
	WeaponData.MaxAmmoInClip = 30;
	WeaponData.StartAmmoInClip = 30;
	WeaponData.WeaponRange = 5000.f;
	WeaponData.ShotsInMinute = 700;
	WeaponData.ProjectileType = EProjectileType::Bullet;
	WeaponData.WeaponSpread = 1;
	WeaponData.Impulse = 12000;

	DeltaTime = 0.f;
	SpeedRotation = 20.f;
}

// Called when the game starts or when spawned
void ATestTurret::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmo = WeaponData.StartAmmo;
	CurrentAmmoInClip = WeaponData.StartAmmoInClip;

	//UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATestTurret::GetClass(), IgnoredActor);

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ATestTurret::SphereCompBeginOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ATestTurret::SphereCompEndOverlap);
}

void ATestTurret::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	DeltaTime = DeltaSeconds;
	
	if (!CurrentTarget)
	{
		SetTarget();
	}
	else
	{
		CastTarget = Cast<ASBaseCharacter>(CurrentTarget);
		if (CastTarget)
		{
			if (!CastTarget->IsAlive())
			{
				StopFire();
				Actors.Remove(CurrentTarget);
				CurrentTarget = nullptr;
			}
			else
			{
				UpdateRotation();
				StartFire();
			}
		}
	}
}

USkeletalMeshComponent * ATestTurret::GetMesh() const
{
	return SkeletalMeshComponent;
}

void ATestTurret::UpdateRotation()
{
	const FVector StartLocation = GetActorLocation();
	const FRotator CurrentRotation = GetActorRotation();
	const FVector TargetLocation = CurrentTarget->GetActorLocation();
	const FRotator AngleRotation = UKismetMathLibrary::FindLookAtRotation(StartLocation, TargetLocation);
	const FRotator NewRotation = FMath::RInterpTo(CurrentRotation, AngleRotation, DeltaTime, SpeedRotation);

	SetActorRotation(NewRotation);
	
}

void ATestTurret::SetTarget()
{
	if (Actors.Num() > 0)
	{
		const uint16 index = FMath::FRandRange(0, Actors.Num());
		CurrentTarget = Actors[index];
	}
}

void ATestTurret::SphereCompBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Actors.Add(OtherActor);
}

void ATestTurret::SphereCompEndOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (Actors.Num() > 0)
	{
		Actors.Remove(OtherActor);
	}
}

void ATestTurret::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SphereComponent->SetSphereRadius(WeaponData.WeaponRange);
}

float ATestTurret::PlayWeaponAnimation(UAnimMontage * Animation, float InPlayRate, FName StartSectionName)
{
	float Duration = 0.f;

	if (Animation)
	{
		//Duration = Mesh->PlayAnimation
	}

	return 0.0f;
}

void ATestTurret::StopWeaponAnimation(UAnimMontage * Animation)
{
	if (Animation)
	{

	}
}

void ATestTurret::Fire()
{
	if (!CanFire())
	{
		if (!CanReload())
		{
			return;
		}
		else
		{
			Reload();
		}
	}

	SimulateWeaponFire();
	Instant_Fire();
	UseAmmo();

	/*switch (WeaponData.ProjectileType)
	{
	case EProjectileType::Bullet:
		SimulateWeaponFire();
		Instant_Fire();
		UseAmmo();
		break;

	case EProjectileType::Spread:
		SimulateWeaponFire();
		for (uint32 i = 0; i < WeaponData.WeaponSpread; i++)
		{
			Instant_Fire();
		}
		UseAmmo();
		break;
	}*/
}

void ATestTurret::Instant_Fire()
{
	const uint32 RandomSeed = FMath::Rand();

	FRandomStream WeaponRandomStream(RandomSeed);

	const float SpreadCone = FMath::DegreesToRadians(WeaponData.WeaponSpread * 0.5);

	const FVector AimDir = GetMesh()->GetSocketRotation("MuzzleFlashSocket").Vector();

	const FVector StartTrace = GetMesh()->GetSocketLocation("MuzzleFlashSocket");

	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, SpreadCone, SpreadCone);

	const FVector EndTrase = StartTrace + ShootDir * WeaponData.WeaponRange;

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrase);

	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, WeaponData.WeaponSpread);

	LastFireTime = GetWorld()->GetTimeSeconds();
}

FHitResult ATestTurret::WeaponTrace(const FVector & TraseFrom, const FVector & TraceTo) const
{
	FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	//TraceParams.AddIgnoredActors(IgnoredActor);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	//TraceParams.AddIgnoredActor(this);

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingleByChannel(Hit, TraseFrom, TraceTo, TURRET_TRACE, TraceParams);

	return Hit;
}

void ATestTurret::ProcessInstantHit(const FHitResult & Impact, const FVector & Origin, const FVector & ShootDir, const uint32 & RandomSeed, const float & ReticleSpread)
{
	const FVector EndTrace = Origin + ShootDir * WeaponData.WeaponRange;
	const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

	DrawDebugLine(GetWorld(), Origin, Impact.TraceEnd, FColor::Red, true, 0.5f, 10.f);

	ASZombieCharacter* Pawn = Cast<ASZombieCharacter>(Impact.GetActor());

	if (Pawn)
	{
		FPointDamageEvent DamageEvent;
		DamageEvent.Damage = WeaponData.Damage;
		DamageEvent.ShotDirection = ShootDir;

		Pawn->GetMesh()->AddImpulseAtLocation(DamageEvent.ShotDirection * WeaponData.Impulse, DamageEvent.HitInfo.ImpactPoint, DamageEvent.HitInfo.BoneName);
		Pawn->TakeDamage(DamageEvent.Damage, DamageEvent, Instigator->GetController(), this);
	}
}

bool ATestTurret::CanFire() const
{
	return CurrentAmmoInClip > 0;
}

bool ATestTurret::CanReload() const
{
	return CurrentAmmo > 0;
}

void ATestTurret::Reload()
{
	const int32 TempCurrentAmmo = CurrentAmmo - WeaponData.MaxAmmoInClip;

	if (TempCurrentAmmo >= 0)
	{
		CurrentAmmo = TempCurrentAmmo;
		CurrentAmmoInClip = WeaponData.MaxAmmoInClip;
	}
	else
	{
		CurrentAmmoInClip = CurrentAmmo;
		CurrentAmmo = 0;
	}
}

void ATestTurret::UseAmmo()
{
	CurrentAmmoInClip--;
}

int32 ATestTurret::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int32 ATestTurret::GetCurrentAmmoInClip() const
{
	return CurrentAmmoInClip;
}

int32 ATestTurret::GetMaxAmmo() const
{
	return WeaponData.MaxAmmo;
}

int32 ATestTurret::GetMaxAmmoInClip() const
{
	return WeaponData.MaxAmmoInClip;
}

void ATestTurret::StartFire()
{
	if (!WeaponFireTimeHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(WeaponFireTimeHandle, this, &ATestTurret::Fire, 60.f / WeaponData.ShotsInMinute, true);
	}
}

void ATestTurret::StopFire()
{
	GetWorldTimerManager().ClearTimer(WeaponFireTimeHandle);
	StopSimulateWeaponFire();
}

void ATestTurret::SimulateWeaponFire()
{
	if (MuzzleFX)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleFX, GetMesh(), MuzzleAttachPoint);
	}

	/*if (!bPlayingFireAnim)
	{
	PlayWeaponAnimation(FireAnim);
	bPlayingFireAnim = true;
	}*/

	PlayWeaponSound(FireSound);
}

void ATestTurret::StopSimulateWeaponFire()
{
	if (bPlayingFireAnim)
	{
		StopWeaponAnimation(FireAnim);
		bPlayingFireAnim = false;
	}
}

UAudioComponent* ATestTurret::PlayWeaponSound(USoundCue * SoundToPlay)
{
	UAudioComponent* AC = nullptr;

	if (SoundToPlay)
	{
		AC = UGameplayStatics::SpawnSoundAttached(SoundToPlay, GetRootComponent());
	}

	return AC;
}

