// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "ZombieAIController.generated.h"

/**
 * 
 */
UCLASS()
class REAPER_API AZombieAIController : public AAIController
{
	GENERATED_BODY()

	class UBlackboardComponent *BlackboardComp;

	class UBehaviorTreeComponent *BehaviorTreeComp;

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName LocationToGoKey;

	UPROPERTY(EditDefaultsOnly, Category = AI)
	FName PlayerKey;

	TArray<AActor *> PatrolPoints;

	virtual void Possess(APawn *Pawn) override;

	virtual void UnPossess() override;

	int32 CurrentPatrolPoint;

public:

	AZombieAIController(const FObjectInitializer& ObjectInitializer);

	void SetPlayerCaught(APawn *Pawn);

	void SetTargetPoint(AActor *target);

	FORCEINLINE UBlackboardComponent *GetBlackboard() const { return BlackboardComp; }

	FORCEINLINE TArray<AActor *> GetPatrolPoints() const { return PatrolPoints; }
};
