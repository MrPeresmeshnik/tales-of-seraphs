// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "SBaseCharacter.h"
#include "SZombieCharacter.generated.h"

/**
 * 
 */
UCLASS(ABSTRACT)
class REAPER_API ASZombieCharacter : public ASBaseCharacter
{
	GENERATED_BODY()

public:
	ASZombieCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, Category = AI)
	class UBehaviorTree *BehaviorTree;

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

private:

	float LastMeleeAttackTime;

	float MeleeStrikeCooldown;

	FTimerHandle TimerHandleMeleeAttack;

	//UPROPERTY(VisibleAnywhere, Category = AI)
	class UPawnSensingComponent* PawnSensingComp;

	UFUNCTION()
	void OnPlayerCaught(APawn *Pawn);

	UPROPERTY(EditDefaultsOnly, Category = Attacking)
	UAnimMontage* MeleeAnimMontage;

	UPROPERTY(VisibleAnywhere, Category = Attacking)
	UCapsuleComponent* MeleeCollisionComp;

	UPROPERTY(EditDefaultsOnly, Category = Attacking)
	float MeleeDamage;

	UPROPERTY(EditDefaultsOnly, Category = Attacking)
	TSubclassOf<UDamageType> MeleeDamageType;

	UFUNCTION()
	void OnMeleeCompBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void PerformMeleeStrike(AActor* HitActor);

	UFUNCTION()
	void OnRetriggerMeleeStrike();

	UFUNCTION()
	void SimulateMeleeStrike();

	void SetRagdollPhysics();

	void Die(const FDamageEvent& DamageEvent);

	void OnDestroy();

private:

	FTimerHandle DestroyTimeHandle;

	UPROPERTY(EditDefaultsOnly, Category = Time, meta = (AllowPrivateAccess = "true"))
	float TimeDisappearance;
};
