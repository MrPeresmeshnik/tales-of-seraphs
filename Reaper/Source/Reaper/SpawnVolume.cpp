// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "SpawnVolume.h"
#include "Kismet/KismetMathLibrary.h"



// Sets default values
ASpawnVolume::ASpawnVolume(const FObjectInitializer &objectInitializer) 
	: Super(objectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	whereToSpawn = objectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WhereToSpaw"));
	RootComponent = whereToSpawn;
	
}

// Called when the game starts or when spawned
void ASpawnVolume::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnVolume::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

UBoxComponent * ASpawnVolume::getWereToSpawn() const
{
	return whereToSpawn;
}

FVector ASpawnVolume::GetRandomPointInVolume() const
{
	FVector SpawnOrigin = whereToSpawn->Bounds.Origin;
	FVector SpawnExtent = whereToSpawn->Bounds.BoxExtent;

	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnExtent);
}

void ASpawnVolume::Spawn(TSubclassOf<AActor> whatToSpawn, const int32 number)
{
	if (whatToSpawn)
	{
		FRotator SpawnRotator;
		SpawnRotator.Pitch = 0;
		SpawnRotator.Roll = 0;
		SpawnRotator.Yaw = 180;

		FVector SpawnLocation;

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		SpawnParameters.Instigator = Instigator;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;

		int32 i = 0;
		while (i < number)
		{
			SpawnLocation = GetRandomPointInVolume();
			
			if (GetWorld()->SpawnActor<AActor>(whatToSpawn, SpawnLocation, SpawnRotator, SpawnParameters))
			{
				i++;
			}
		}
	}
}

AActor *ASpawnVolume::Spawn2(TSubclassOf<AActor> whatToSpawn)
{
	if (whatToSpawn)
	{
		FRotator SpawnRotator;
		SpawnRotator.Pitch = 0;
		SpawnRotator.Roll = 0;	
		SpawnRotator.Yaw = 180;

		FVector SpawnLocation = GetActorLocation();

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = Instigator;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;

		//SpawnLocation = GetRandomPointInVolume();

		return GetWorld()->SpawnActor<AActor>(whatToSpawn, SpawnLocation, SpawnRotator, SpawnParameters);
	}

	return nullptr;
}
