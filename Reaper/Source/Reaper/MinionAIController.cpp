// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "MinionAIController.h"

void AMinionAIController::SetTargetPoint(AActor * target)
{
	if (target)
	{
		MoveToLocation(target->GetActorLocation(), 10);
	}
}
