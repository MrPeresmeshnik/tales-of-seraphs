// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "SBaseCharacter.h"


// Sets default values
ASBaseCharacter::ASBaseCharacter(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	Health = 100;

}

// Called when the game starts or when spawned
void ASBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASBaseCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ASBaseCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

float ASBaseCharacter::GetMaxHealth() const
{
	return GetClass()->GetDefaultObject<ASBaseCharacter>()->Health;
}

float ASBaseCharacter::GetHealth() const
{
	return Health;
}

bool ASBaseCharacter::IsAlive() const
{
	return Health > 0;
}