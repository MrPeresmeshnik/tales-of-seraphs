// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "TestTurret.generated.h"

UENUM()
enum class EProjectileType
{
	Bullet,
	Spread
};

USTRUCT()
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 Damage;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 MaxAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 StartAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 MaxAmmoInClip;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 StartAmmoInClip;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	uint32 ShotCost;

	/*UPROPERTY(EditDefaultsOnly, Category = Config)
	float TimeBetweenShots;*/

	UPROPERTY(EditDefaultsOnly, Category = Config)
		uint32 ShotsInMinute;

	UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponRange;

	UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponSpread;

	UPROPERTY(EditDefaultsOnly, Category = Config)
		float Impulse;

	UPROPERTY(EditDefaultsOnly, Category = Config)
		TEnumAsByte<EProjectileType> ProjectileType;
};

UCLASS()
class REAPER_API ATestTurret : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATestTurret(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

protected:
	USkeletalMeshComponent* GetMesh() const;

	UFUNCTION(BlueprintCallable, Category = Rotation, meta = (AllowPrivateAccess = "true"))
	void UpdateRotation();

	UPROPERTY(Category = Config, EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
 	float SpeedRotation;

	void SetTarget();

	float DeltaTime;

	class ASBaseCharacter *CastTarget;

private:

	TArray<AActor *> Actors;

	TArray<AActor *> IgnoredActor;

	UPROPERTY(Category = Setting, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	AActor *CurrentTarget;

	UFUNCTION()
	void SphereCompBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
	void SphereCompEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	virtual void PostInitializeComponents() override;

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SphereComponent;

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent *SkeletalMeshComponent;

	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* CapsuleComponent;

	uint32 CurrentAmmoInClip;

	uint32 CurrentAmmo;

	float LastFireTime;

	FTimerHandle WeaponFireTimeHandle;

	UPROPERTY(EditDefaultsOnly)
		FName MuzzleAttachPoint;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* EquipAnim;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* FireAnim;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
		USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
		USoundCue* EquipSound;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* MuzzleFX;

	float PlayWeaponAnimation(UAnimMontage* Animation, float InPlayRate = 1.f, FName StartSectionName = NAME_None);

	void StopWeaponAnimation(UAnimMontage* Animation);

	UPROPERTY(EditDefaultsOnly, Category = Config, meta = (AllowPrivateAccess = "true"))
	FWeaponData WeaponData;

	UFUNCTION()
		void Instant_Fire();

	FHitResult WeaponTrace(const FVector& TraseFrom, const FVector& TraceTo) const;

	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, const uint32& RandomSeed, const float& ReticleSpread);

	void Fire();

	bool CanFire() const;

	bool CanReload() const;

	void Reload();

	void UseAmmo();

	bool bPlayingFireAnim;

	void SimulateWeaponFire();

	void StopSimulateWeaponFire();

	UAudioComponent* PlayWeaponSound(USoundCue* SoundToPlay);

public:
	UFUNCTION(BlueprintCallable, Category = Ammo)
		int32 GetCurrentAmmo() const;

	UFUNCTION(BlueprintCallable, Category = Ammo)
		int32 GetCurrentAmmoInClip() const;

	UFUNCTION(BlueprintCallable, Category = Ammo)
		int32 GetMaxAmmo() const;

	UFUNCTION(BlueprintCallable, Category = Ammo)
		int32 GetMaxAmmoInClip() const;

	FORCEINLINE uint32 GetShotsInMinute() const { return WeaponData.ShotsInMinute; }

	UFUNCTION(BlueprintCallable, Category = Weapon)
		void StartFire();

	UFUNCTION(BlueprintCallable, Category = Weapon)
		void StopFire();
	
};
