// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "SWeapon.h"



void ASWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	TimeBetweenShots = 60.0f / ShotsPerMinute;
	CurrentAmmo = FMath::Min(StartAmmo, MaxAmmo);
	CurrentAmmoInClip = FMath::Min(MaxAmmoPerClip, StartAmmo);
}

void ASWeapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	StopSimulatingWeaponFire();
}

float ASWeapon::GetEquipStartedTime() const
{
	return EquipStartedTime;
}

float ASWeapon::GetEquipDuration() const
{
	return EquipDuration;
}

// Sets default values
ASWeapon::ASWeapon(const FObjectInitializer &objectInitializer)
	: Super(objectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;

	/*Mesh = objectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	Mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh->bReceivesDecals = true;
	Mesh->CastShadow = true;
	Mesh->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	RootComponent = Mesh;*/

	bIsEquipped = false;

	CurrentState = EWeaponState::Idle;

	MuzzleAttachPoint = TEXT("MuzzleFlashSocket");

	ShotsPerMinute = 700;
	StartAmmo = 999;
	MaxAmmo = 999;
	MaxAmmoPerClip = 30;
	NoAnimReloadDuration = 1.5f;
	NoEquipAnimDuration = 0.5f;

}

// Called when the game starts or when spawned
void ASWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ASWeapon::OnEquipFinished()
{
}

bool ASWeapon::IsEquipped() const
{
	return bIsEquipped;
}

bool ASWeapon::IsAttachedToPawn() const
{
	return false;
}

/*USkeletalMeshComponent * ASWeapon::GetWeaponMesh() const
{
	return Mesh;
}*/

void ASWeapon::OnUnEquip()
{
}

void ASWeapon::OnEquip(bool bPlayAnimation)
{
}

void ASWeapon::StartFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		DetermineWeaponState();
	}
}

void ASWeapon::StopFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		DetermineWeaponState();
	}
}

EWeaponState ASWeapon::GetCurrentState() const
{
	return CurrentState;
}

bool ASWeapon::CanFire() const
{
	const bool bStateOk = CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing;
	return bStateOk && !bPendingReload;
}

FVector ASWeapon::GetAdjustedAim() const
{
	return FVector();
}

FVector ASWeapon::GetCameraDamageStartLocation(const FVector & AimDir) const
{
	return FVector();
}

FHitResult ASWeapon::WeaponTrace(const FVector & TraceFrom, const FVector & TraceTo) const
{
	FCollisionQueryParams TraceParams(TEXT("WeaponTrace"), true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, ECC_GameTraceChannel1, TraceParams);

	return Hit;
}

void ASWeapon::SetWeaponState(EWeaponState NewState)
{
	const EWeaponState PrevState = CurrentState;

	if (PrevState == EWeaponState::Firing && NewState != EWeaponState::Firing)
	{
		OnBurstFinished();
	}

	CurrentState = NewState;

	if (PrevState != EWeaponState::Firing && NewState == EWeaponState::Firing)
	{
		OnBurstFinished();
	}
}

void ASWeapon::DetermineWeaponState()
{
	EWeaponState NewState = EWeaponState::Idle;

	if (bIsEquipped)
	{
		if (bPendingReload)
		{
			if (CanReload())
			{
				NewState = EWeaponState::Reloading;
			}
			else
			{
				NewState = CurrentState;
			}
		}
		else if (!bPendingReload && bWantsToFire && CanFire())
		{
			NewState = EWeaponState::Firing;
		}
	}
	else if (bPendingEquip)
	{
		NewState = EWeaponState::Equipping;
	}

	SetWeaponState(NewState);
}

void ASWeapon::HandleFiring()
{
	if (CurrentAmmoInClip > 0 && CanFire())
	{
		FireWeapon();
		UseAmmo();
	}
	else
	{
		if (CanReload())
		{
			StartReload();
		}
		else
		{
			if (GetCurrentAmmo() == 0 && !bRefiring)
			{
				PlayWeaponSound(OutOfAmmoSound);
			}
			if (CurrentAmmoInClip <= 0 && CanReload())
			{
				StartReload();
			}
		}
	}

	bRefiring = (CurrentState == EWeaponState::Firing && TimeBetweenShots > 0.0f);
	if (bRefiring)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &ASWeapon::HandleFiring, TimeBetweenShots, false);
	}

	LastFireTime = GetWorld()->GetTimeSeconds();
}

void ASWeapon::OnBurstStarted()
{
	const float GameTime = GetWorld()->GetTimeSeconds();
	if (LastFireTime > 0 && TimeBetweenShots > 0 && LastFireTime + TimeBetweenShots > GameTime)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &ASWeapon::HandleFiring, LastFireTime + TimeBetweenShots - GameTime, false);
	}
	else
	{
		HandleFiring();
	}
}

void ASWeapon::OnBurstFinished()
{
	StopSimulatingWeaponFire();

	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	bRefiring = false;
}

void ASWeapon::OnRep_BurstCounter()
{
}

void ASWeapon::SimulateWeaponFire()
{
	if (MuzzleFX)
	{
		MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, GetMesh(), MuzzleAttachPoint);
	}

	if (!bPlayingFireAnim)
	{
		PlayWeaponAnimation(FireAnim);
		bPlayingFireAnim = true;
	}

	PlayWeaponSound(FireSound);
}

void ASWeapon::StopSimulatingWeaponFire()
{
	if (bPlayingFireAnim)
	{
		StopWeaponAnimation(FireAnim);
		bPlayingFireAnim = false;
	}
}

FVector ASWeapon::GetMuzzleLocation() const
{
	return GetMesh()->GetSocketLocation(MuzzleAttachPoint);
}

FVector ASWeapon::GetMuzzleDirection() const
{
	return GetMesh()->GetSocketRotation(MuzzleAttachPoint).Vector();
}

UAudioComponent * ASWeapon::PlayWeaponSound(USoundCue * SoundToPlay)
{
	UAudioComponent* AC = nullptr;
	if (SoundToPlay)
	{
		AC = UGameplayStatics::SpawnSoundAttached(SoundToPlay, GetRootComponent());
	}

	return AC;
}

float ASWeapon::PlayWeaponAnimation(UAnimMontage * Animation, float InPlayRate, FName StartSectionName)
{
	float Duration = 0.0f;
	if (Animation)
	{
		//Duration = PlayAnimMontage(Animation, InPlayRate, StartSectionName);
	}


	return Duration;
}

void ASWeapon::StopWeaponAnimation(UAnimMontage * Animation)
{
}

void ASWeapon::UseAmmo()
{
	CurrentAmmoInClip--;
	CurrentAmmo--;
}

void ASWeapon::ReloadWeapon()
{
	int32 ClipDelta = FMath::Min(MaxAmmoPerClip - CurrentAmmoInClip, CurrentAmmo - CurrentAmmoInClip);

	if (ClipDelta > 0)
	{
		CurrentAmmoInClip += ClipDelta;
	}
}

bool ASWeapon::CanReload()
{
	bool bGotAmmo = (CurrentAmmoInClip < MaxAmmoPerClip) && ((CurrentAmmo - CurrentAmmoInClip) > 0);
	bool bStateOKToReload = ((CurrentState == EWeaponState::Idle) || (CurrentState == EWeaponState::Firing));
	return (bGotAmmo && bStateOKToReload);
}

void ASWeapon::OnRep_Reload()
{
	if (bPendingReload)
	{
		/* By passing true we do not push back to server and execute it locally */
		StartReload(true);
	}
	else
	{
		StopSimulateReload();
	}
}

void ASWeapon::StartReload(bool bFromReplication)
{
	if (CanReload())
	{
		bPendingReload = true;
		DetermineWeaponState();

		float AnimDuration = PlayWeaponAnimation(ReloadAnim);
		if (AnimDuration <= 0.0f)
		{
			AnimDuration = NoAnimReloadDuration;
		}

		GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &ASWeapon::StopSimulateReload, AnimDuration, false);
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &ASWeapon::ReloadWeapon, FMath::Max(0.1f, AnimDuration - 0.1f), false);
		}

		PlayWeaponSound(ReloadSound);

	}
}

void ASWeapon::StopSimulateReload()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bPendingReload = false;
		DetermineWeaponState();
		StopWeaponAnimation(ReloadAnim);
	}
}

int32 ASWeapon::GiveAmmo(int32 AddAmount)
{
	const int32 MissingAmmo = FMath::Max(0, MaxAmmo - CurrentAmmo);
	AddAmount = FMath::Min(AddAmount, MissingAmmo);
	CurrentAmmo += AddAmount;

	/* Push reload request to client */
	if (GetCurrentAmmoInClip() <= 0 && CanReload())
	{
		StartReload();
	}

	/* Return the unused ammo when weapon is filled up */
	return FMath::Max(0, AddAmount - MissingAmmo);
}

void ASWeapon::SetAmmoCount(int32 NewTotalAmount)
{
	CurrentAmmo = FMath::Min(MaxAmmo, NewTotalAmount);
	CurrentAmmoInClip = FMath::Min(MaxAmmoPerClip, CurrentAmmo);
}

int32 ASWeapon::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int32 ASWeapon::GetCurrentAmmoInClip() const
{
	return CurrentAmmoInClip;
}

int32 ASWeapon::GetMaxAmmoPerClip() const
{
	return MaxAmmoPerClip;
}

int32 ASWeapon::GetMaxAmmo() const
{
	return MaxAmmo;
}
