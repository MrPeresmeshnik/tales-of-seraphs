// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "Turrent.h"



void ATurrent::SpawnImpactEffects(const FHitResult & Impact)
{
}

void ATurrent::SpawnTrailEffects(const FVector & EndPoint)
{
}

ATurrent::ATurrent(const FObjectInitializer &objectInitializer)
	:Super(objectInitializer)
{
	HitDamage = 26;
	WeaponRange = 15000;

	AllowedViewDotHitDir = -1.0f;
	ClientSideHitLeeway = 200.0f;
	MinimumProjectileSpawnDistance = 800;
	TracerRoundInterval = 3;
}

void ATurrent::FireWeapon()
{
	const FVector AimDir = GetAdjustedAim();
	const FVector CameraPos = GetCameraDamageStartLocation(AimDir);
	const FVector EndPos = CameraPos + (AimDir * WeaponRange);

	FHitResult Impact = WeaponTrace(CameraPos, EndPos);

	const FVector MuzzleOrigin = GetMuzzleLocation();

	FVector AdjustedAimDir = AimDir;
	if (Impact.bBlockingHit)
	{
		/* Adjust the shoot direction to hit at the crosshair. */
		AdjustedAimDir = (Impact.ImpactPoint - MuzzleOrigin).GetSafeNormal();

		/* Re-trace with the new aim direction coming out of the weapon muzzle */
		Impact = WeaponTrace(MuzzleOrigin, MuzzleOrigin + (AdjustedAimDir * WeaponRange));
	}
	else
	{
		/* Use the maximum distance as the adjust direction */
		Impact.ImpactPoint = FVector_NetQuantize(EndPos);
	}

	ProcessInstantHit(Impact, MuzzleOrigin, AdjustedAimDir);
}

void ATurrent::DealDamage(const FHitResult & Impact, const FVector & ShootDir)
{
	float ActualHitDamage = HitDamage;

	FPointDamageEvent PointDmg;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = ShootDir;
	PointDmg.Damage = ActualHitDamage;

	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, GetController(), this);

}

bool ATurrent::ShouldDealDamage(AActor * TestActor) const
{
	if (TestActor)
	{
		return true;
	}

	return false;
}

void ATurrent::ProcessInstantHit(const FHitResult & Impact, const FVector & Origin, const FVector & ShootDir)
{
}

void ATurrent::ProcessInstantHitConfirmed(const FHitResult & Impact, const FVector & Origin, const FVector & ShootDir)
{
}

void ATurrent::OnRep_HitLocation()
{
}
