// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "ZombieAIController.h"
#include "SZombieCharacter.h"
#include "BehaviorTree/BehaviorTree.h"



void AZombieAIController::Possess(APawn * Pawn)
{
	Super::Possess(Pawn);

	ASZombieCharacter *ZombieAI = Cast<ASZombieCharacter>(Pawn);

	if (ZombieAI && ZombieAI->BehaviorTree)
	{
		BlackboardComp->InitializeBlackboard(*(ZombieAI->BehaviorTree->BlackboardAsset));

		//BehaviorTreeComp->StartTree(*(ZombieAI->BehaviorTree));
	}
}

void AZombieAIController::UnPossess()
{
	Super::UnPossess();
	if (BehaviorTreeComp)
	{
		BehaviorTreeComp->StopTree();
	}
	
}

AZombieAIController::AZombieAIController(const FObjectInitializer & ObjectInitializer)
{
	BehaviorTreeComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorTreeComp"));
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));

	LocationToGoKey = "LocationToGo";
	PlayerKey = "Target";

	CurrentPatrolPoint = 0;
}

void AZombieAIController::SetPlayerCaught(APawn * Pawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(PlayerKey, Pawn);
	}
}

void AZombieAIController::SetTargetPoint(AActor * target)
{
	if (target)
	{
		MoveToLocation(target->GetActorLocation(), 10.f);
		//BlackboardComp->SetValueAsObject(LocationToGoKey, target);
	}
}
