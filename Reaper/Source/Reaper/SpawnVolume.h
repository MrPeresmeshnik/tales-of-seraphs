// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"


UCLASS()
class REAPER_API ASpawnVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnVolume(const FObjectInitializer &objectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	FORCEINLINE UBoxComponent* getWereToSpawn() const;

	UFUNCTION(BlueprintPure, Category = Spawning)
	FVector GetRandomPointInVolume() const;

	UFUNCTION(BlueprintCallable, Category = Spawining)
	void Spawn(TSubclassOf<AActor> whatToSpawn, const int32 number);

	UFUNCTION(BlueprintCallable, Category = Spawining)
	AActor *Spawn2(TSubclassOf<AActor> whatToSpawn);

	UPROPERTY(EditAnywhere, Category = Spawning)
	TArray<AActor *> arrayTarget;

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	UBoxComponent *whereToSpawn;
};

