// Fill out your copyright notice in the Description page of Project Settings.

#include "Reaper.h"
#include "SZombieCharacter.h"
#include "ZombieAIController.h"
#include "SpawnVolume.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/PawnSensingComponent.h"



ASZombieCharacter::ASZombieCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	PawnSensingComp = ObjectInitializer.CreateDefaultSubobject<UPawnSensingComponent>(this, TEXT("PawnSensingComp"));
	PawnSensingComp->SetPeripheralVisionAngle(90.f);

	MeleeCollisionComp = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("MeleeCollision"));
	MeleeCollisionComp->SetRelativeLocation(FVector(45, 0, 25));
	MeleeCollisionComp->SetCapsuleHalfHeight(60);
	MeleeCollisionComp->SetCapsuleRadius(35, false);
	MeleeCollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	MeleeCollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	MeleeCollisionComp->SetupAttachment(GetRootComponent());

	Health = 100;
	MeleeDamage = 24;
	MeleeStrikeCooldown = 1.f;

	TimeDisappearance = 5.f;
}

void ASZombieCharacter::BeginPlay()
{
	Super::BeginPlay();

	ASpawnVolume *spawner = Cast<ASpawnVolume>(this->GetOwner());
	AZombieAIController *controller = Cast<AZombieAIController>(GetController());

	if (controller && spawner && spawner->arrayTarget.Num() > 0)
	{
		const uint32 index = FMath::FRandRange(0, spawner->arrayTarget.Num());
		AActor *target = spawner->arrayTarget[index];

		if (target)
		{
			controller->SetTargetPoint(target);
		}
	}
}

void ASZombieCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

float ASZombieCharacter::TakeDamage(float Damage, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	if (Health <= 0.f)
	{
		return 0.0f;
	}

	//const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	Health -= Damage;

	if (Health <= 0.f)
	{
		Die(DamageEvent);

		if (!DestroyTimeHandle.IsValid())
		{
			GetWorldTimerManager().SetTimer(DestroyTimeHandle, this, &ASZombieCharacter::OnDestroy, TimeDisappearance);
		}
		//Destroy();
	}
	else
	{

	}

	return Damage;
}

void ASZombieCharacter::OnPlayerCaught(APawn * Pawn)
{
}

void ASZombieCharacter::OnMeleeCompBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}

void ASZombieCharacter::PerformMeleeStrike(AActor * HitActor)
{
}

void ASZombieCharacter::OnRetriggerMeleeStrike()
{

}
void ASZombieCharacter::SimulateMeleeStrike()
{

}

void ASZombieCharacter::SetRagdollPhysics()
{
	USkeletalMeshComponent* MeshComp = GetMesh();
	if (MeshComp)
	{
		MeshComp->SetAllBodiesSimulatePhysics(true);
		MeshComp->SetSimulatePhysics(true);
		MeshComp->WakeAllRigidBodies();
		MeshComp->bBlendPhysics = true;
	}

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}
}

void ASZombieCharacter::Die(const FDamageEvent & DamageEvent)
{
	DetachFromControllerPendingDestroy();

	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	USkeletalMeshComponent* Mesh3P = GetMesh();
	if (Mesh3P)
	{
		Mesh3P->SetCollisionProfileName(TEXT("Ragdoll"));
	}
	SetActorEnableCollision(true);

	SetRagdollPhysics();

	FPointDamageEvent PointDamage = *((FPointDamageEvent*)(&DamageEvent));
	{
		Mesh3P->AddImpulseAtLocation(PointDamage.ShotDirection * 12000, PointDamage.HitInfo.ImpactPoint, PointDamage.HitInfo.BoneName);
	}
}

void ASZombieCharacter::OnDestroy()
{
	Destroy();
}
