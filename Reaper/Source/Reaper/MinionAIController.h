// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "MinionAIController.generated.h"

/**
 * 
 */
UCLASS()
class REAPER_API AMinionAIController : public AAIController
{
	GENERATED_BODY()
	
public:		
	void SetTargetPoint(AActor *target);
	
	
};
