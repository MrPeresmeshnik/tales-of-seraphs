// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SBaseCharacter.h"
#include "SMinionCharacter.generated.h"

/**
 * 
 */
UCLASS()
class REAPER_API ASMinionCharacter : public ASBaseCharacter
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = Movement)
	void MoveToTarget(AActor *target);
	
	
};
